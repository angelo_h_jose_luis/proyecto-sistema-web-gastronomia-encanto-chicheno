import React,{Component}from 'react';
import { Meteor } from 'meteor/meteor';
import {Tracker} from 'meteor/tracker'
import { render } from 'react-dom';
import {routes,checkAuthUser} from '/imports/routes/Routes' // array de rutas para la navegacion
import Principal from '/imports/view/principal/Principal'
import SwitchRoutes from '/imports/routes/SwitchRoutes'// creado para renderizar nuestras subrutas
import {BrowserRouter,Route,Switch,Redirect} from 'react-router-dom'
//moment = require('moment')
T9n = require ('meteor-accounts-t9n').T9n

Tracker.autorun(function(){
  T9n.map('es', require('meteor-accounts-t9n/build/es'))
  T9n.setLanguage('es')

  const authenticated = !!Meteor.userId()
  //console.log(`el id de usuario es ${authenticated}`)
  checkAuthUser(authenticated)

  if(!authenticated){
    Meteor.logout(function(err) {
        //console.log('cerrando Session')
    }); 
  }
})

class App extends Component {
  render() {
    return (
      <BrowserRouter>
          <Switch>
              {/*<Route exact path="/" component={Principal} />*/}
              <Route exact path="/">
                  <Redirect to="/bienvenido/principal"/>
              </Route>
              <Route exact path="/bienvenido">
                  <Redirect to="/bienvenido/principal"/>
              </Route>
              {                
                routes.map((route,i)=>{
                  return <SwitchRoutes key={i} {...route}/>
                })
              }
          </Switch>
      </BrowserRouter>
    )
  }
}


Meteor.startup(() => {
  render(<App/>, document.getElementById('aqui_se_renderizara_mi_proyecto'));
});