export default UserFiles = new FilesCollection({
    //storagePath: '/sis414/files',
    storagePath: '../../../../../sis414/files',
    downloadRoute: '/files/sis414',
    collectionName: 'UserFiles',
    permissions: 0o755,
    allowClientCode: false, // Disallow remove files from Client
    cacheControl: 'public, max-age=31536000',
    onbeforeunloadMessage() {
        return 'Upload is still in progress! Upload will be aborted if you leave this page!';
    },
    onBeforeUpload(file) {
        // Allow upload files under 10MB, and only in png/jpg/jpeg formats
        // Note: You should never trust to extension and mime-type here
        // as this data comes from client and can be easily substitute
        // to check file's "magic-numbers" use `mmmagic` or `file-type` package
        // real extension and mime-type can be checked on client (untrusted side)
        // and on server at `onAfterUpload` hook (trusted side)
        if (file.size <= 10485760 && /png|mp4|jpe?g/i.test(file.ext)) {
          return true;
        }
        return 'Please upload image, with size equal or less than 10MB';
    },
    downloadCallback(fileObj) {
        if (this.params.query.download == 'true') {
          // Increment downloads counter
          UserFiles.update(fileObj._id, {$inc: {'meta.downloads': 1}});
        }
        // Must return true to continue download
        return true;
    }
});