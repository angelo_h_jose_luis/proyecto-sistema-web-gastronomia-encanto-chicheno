Meteor.methods({
    userCreate:function(form){
        
        //return 'hola mundo desde el servidor'
        if(form.password != form.re_password)
            throw new Meteor.Error(406, "La constraseña debe ser igual a la confirmacion de contraseña");
        const _id = Accounts.createUser({
            createdAt:new Date(),//fecha de creacion del usuario,
            username:form.username,
            email:form.email,
            password:form.password,
            profile:{
                fullname:form.nameComplete
            }
        })
        
        const roles = Meteor.roles.find({group:'people'}).fetch()
        const rolesuser = []
        roles.forEach(element => {
            rolesuser.push(element._id)
        });
        Roles.addUsersToRoles(_id,rolesuser)
        return 'Usuario Creado correctamente'
    }
})

Meteor.methods({
    createRoles(){
        ///ESTO SE PUEDE HACER DE FORMA DINAMICA DEPENDIENDO EL DESARROLLO DE SU SISTEMA
        /*Meteor.roles.insert({
            _id:'createpublication',//el permiso o rol
            description:'Crear Publicacion',
            groupdescription:'publicaciones',
            group:'people',
            children:[],
            key:new Meteor.Collection.ObjectID()
        })
        Meteor.roles.insert({_id:'allpublicationuser',description:'Ver mis Publicacion',groupdescription:'publicaciones',group:'people',children:[],key:new Meteor.Collection.ObjectID()})
        Meteor.roles.insert({_id:'editpublication',description:'Editar Publicacion',groupdescription:'publicaciones',group:'people',children:[],key:new Meteor.Collection.ObjectID()})
        Meteor.roles.insert({_id:'downpublication',description:'Eliminar Publicacion',groupdescription:'publicaciones',group:'people',children:[],key:new Meteor.Collection.ObjectID()})
        Meteor.roles.insert({_id:'createCategory',description:'Crear Categoria',groupdescription:'categorias',group:'admin',children:[],key:new Meteor.Collection.ObjectID()})
        Meteor.roles.insert({_id:'viewallCategory',description:'Ver Categorias',groupdescription:'categorias',group:'admin',children:[],key:new Meteor.Collection.ObjectID()})*/

        Roles.setUserRoles(Meteor.userId(),["createpublication","allpublicationuser","editpublication","downpublication","createCategory","viewallCategory"])
        return 'creado correactamente'
    }
})

Meteor.publish(null, function () {
    if (this.userId) {
      return Meteor.roleAssignment.find({ 'user._id': this.userId });
    } else {
      this.ready()
    }
})