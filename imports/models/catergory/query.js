import {categoryClass} from './class'

export const queryCategorys = class{
    constructor(options,mthis){
        this.options = options
        this.mthis = mthis
    }
    getCategory(){
        try {
            return categoryClass.find({active:true},{sort:{createdAt:-1}})
        } catch (error) {
            console.log(error)
        }
    }
    getAllCategory(){
        try {
            return categoryClass.find({},{sort:{createdAt:-1}})
        } catch (error) {
            console.log(error)
        }
    }
}