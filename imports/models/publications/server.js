import {publicationsClass} from './class'
import {categoryClass} from '../catergory/class'
import UserFiles from '../files/class'
import {queryPublications} from './querys'

publicationsClass.extend({
    meteorMethods:{
        newPublication(form){
            try {
                if(Roles.userIsInRole(Meteor.userId(),"createpublication")){
                        //console.log(form
                        const file = UserFiles.findOne({_id:form.image})
                        const URL = file.link()
                        const indextext = file.mime.lastIndexOf('/')
                        var newUrl = URL.replace(/^.*\/\/[^\/]+/,'')
                        const category = categoryClass.findOne({_id:new Meteor.Collection.ObjectID(form.category)})
                        this.title = form.title
                        this.description = form.description
                        this.phone = form.phone
                        this.price = parseInt(form.price) /// en el cliente esto es "precio" y en el squema es "price")
                        this.nameCategory = category.name
                        this.idCategory = category._id
                        this.idfile = form.image
                        this.urlfile = newUrl
                        this.startDate = form.startDate
                        this.endDate = form.endDate
                        this.user = Meteor.userId()
                        this.username = Meteor.user().username
                        this.created_view = new Date()
                        this.typepub = file.mime.slice(0,indextext)
                        this.save()
                        return "Guardado Correctamente"
                }else{
                    throw new Meteor.Error(403,'ACCESO DENEGADO')
                }
            } catch (error) {
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }
            //console.log(this)

            ///// metodo para remplazar las mismas propiedades o campos que llegan desde el cliente/////
            //this.set(form) //esto cambia los valores de las propiedadades son los mismos nombres que llegan desde el cliente

            ///// metodo para remplazar diferentes propiedades o campos que llegan desde el cliente/////
            ///////////
            /*this.title = form.title
            this.description = form.description
            this.phone = form.phone
            this.price = parseInt(form.precio) /// en el cliente esto es "precio" y en el squema es "price")
            this.startDate = form.startDate
            this.endDate = form.endDate*/
            ////////////

            //this.save()
           
        },
        editPublication(form){
            try {
                if(form.image){
                    //console.log(form
                    const URL = UserFiles.findOne({_id:form.image}).link()
                    var newUrl = URL.replace(/^.*\/\/[^\/]+/,'')
                    const category = categoryClass.findOne({_id:new Meteor.Collection.ObjectID(form.category)})
                    this.title = form.title
                    this.description = form.description
                    this.phone = form.phone
                    this.price = parseInt(form.price) /// en el cliente esto es "precio" y en el squema es "price")
                    this.nameCategory = category.name
                    this.idCategory = category._id
                    this.idfile = form.image
                    this.urlfile = newUrl
                    this.startDate = form.startDate
                    this.endDate = form.endDate
                    this.user = Meteor.userId()
                    this.username = Meteor.user().username
                    this.created_view = new Date()
                    this.save()
                    return "editado Correctamente con imagen"
                }else{
                        console.log(form.price)
                        //const URL = UserFiles.findOne({_id:form.image}).link()
                        //var newUrl = URL.replace(/^.*\/\/[^\/]+/,'')
                        const category = categoryClass.findOne({_id:new Meteor.Collection.ObjectID(form.category)})
                        this.title = form.title
                        this.description = form.description
                        this.phone = form.phone
                        this.price = parseInt(form.price) /// en el cliente esto es "precio" y en el squema es "price")
                        this.nameCategory = category.name
                        this.idCategory = category._id
                        //this.idfile = form.image
                        //this.urlfile = newUrl
                        this.startDate = form.startDate
                        this.endDate = form.endDate
                        this.user = Meteor.userId()
                        this.username = Meteor.user().username
                        this.created_view = new Date()
                        this.save()
                        return "editado Correctamente SIN imagen"
                }
            } catch (error) {
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }           
        },
        updateStatePublication(){
            try{
                this.active = this.active?false:true
                return this.save()
            }catch(error){
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }
        }
    }
})

//aqui el cliente se subcribira para poder acceder a los datos de la base de datos
//Meteor.publish('mispublicaciones',function(){
    // publicationsClass.find() selecioname todo de la (colleccion) publications
    //return publicationsClass.find()//s
    ///ESTO ESTA PROHIBIDO HACER
    ///imaginamemos que tenemos 3000000 de datos
    ///la carga de datos seria fatal
//})

Meteor.publish('publications',function(options,type){
    try {
        const subs = new queryPublications(options,this)
        return subs[type]()
    } catch (error) {
        this.stop()
        throw new Meteor.Error(403,error.reason)
    }
})