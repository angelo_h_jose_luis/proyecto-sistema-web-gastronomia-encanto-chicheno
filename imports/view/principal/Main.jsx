import React, { Component } from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import {Link,Switch} from 'react-router-dom'
import {publicationsClass} from '../../models/publications/class'
import {categoryClass} from '../../models/catergory/class'

const categorySelect = new ReactiveVar(undefined)

class Main extends Component {
    selectCategory = (category) => {
        categorySelect.set(category?category:undefined)
    }
    render() {
        const {publications,subscriptionPublications,categorys,publicationsLimitTwo} = this.props
        return (
            <div>
                <div className="slider-area slider-bg ">
                    <div className="slider-active">

                        <div className="single-slider d-flex align-items-center slider-height ">
                            <div className="container">
                                <div className="row align-items-center justify-content-between">
                                    <div className="col-xl-5 col-lg-5 col-md-9 ">
                                        <div className="hero__caption">
                                            <span data-animation="fadeInLeft" data-delay=".3s">Mejor servicio & cálida bienvenida</span>
                                            <h1 data-animation="fadeInLeft" data-delay=".6s ">Sistema web Gastronomía Encanto Chicheño
                                            </h1>
                                            <p data-animation="fadeInLeft" data-delay=".8s">Ven y prueba nuestras deliciosas variedades típicas, en sabores y mucho más de nuestras tradiciones, culturas regionales.</p>

                                            <div className="slider-btns">
                                                <Link  data-animation="fadeInLeft" data-delay="1s" className="btn radius-btn" to="/bienvenido/login">Iniciar Sesion</Link>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6">
                                        <div className="hero__img d-none d-lg-block f-right">
                                            <img src="/principal/img/hero_right.png" data-animation="fadeInRight"
                                                data-delay="1s" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="single-slider d-flex align-items-center slider-height ">
                            <div className="container">
                                <div className="row align-items-center">
                                    <div className="col-lg-6 col-md-9 ">
                                        <div className="hero__caption">
                                            <span data-animation="fadeInLeft" data-delay=".3s">Best Domain & hosting
                                                service provider</span>
                                            <h1 data-animation="fadeInLeft" data-delay=".6s">Powerful web hosting
                                            </h1>
                                            <p data-animation="fadeInLeft" data-delay=".8s">Supercharge your
                                                WordPress hosting with detailed
                                                website analytics, marketing tools, security, and data
                                                backups all in one place.</p>

                                            <div className="slider-btns">

                                                <a data-animation="fadeInLeft" data-delay="1s" href="industries.html"
                                                    className="btn radius-btn">get started</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="hero__img d-none d-lg-block f-right">
                                            <img src="/principal/img/hero_right.png" data-animation="fadeInRight"
                                                data-delay="1s" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="slider-shape d-none d-lg-block">
                        <img className="slider-shape1" src="/principal/img/top-left-shape.png" />
                    </div>
                </div>

                <section className="blog_area section-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 mb-5 mb-lg-0">
                                <div className="blog_left_sidebar">
                                    {
                                        publications.map((data,key)=>{
                                            return <article key={`article_${key}`} className="blog_item">
                                                        <div className="blog_item_img">
                                                            {data.typepub == 'video'? 
                                                                (
                                                                   <video className="card-img rounded-0"  controls>
                                                                        <source src={data.urlfile}/>
                                                                    </video>
                                                                )
                                                                :
                                                                <img className="card-img rounded-0" src={data.urlfile}  />
                                                            }
                                                            <a href="#" className="blog_item_date">
                                                                <h3>{moment(data.created_view).format('DD')}</h3>
                                                                <p>{moment(data.created_view).format('MMM')}</p>
                                                            </a>
                                                        </div>
                                                        <div className="blog_details">
                                                            <a className="d-inline-block" href="blog_details.html">
                                                                <h2 className="blog-head" style={{ color: '#2d2d2d' }}>{data.title}</h2>
                                                            </a>
                                                            <p>{data.description}</p>
                                                            <ul className="blog-info-link">
                                                                <li><a href="#"><i className="fa fa-user" />{data.username}</a></li>
                                                                <li><a href="#"><i className="fa fa-comments" /> 03 Comments</a></li>
                                                            </ul>
                                                        </div>
                                                    </article>
                                        })
                                    }

                                   <nav className="blog-pagination justify-content-center d-flex">
                                        <ul className="pagination">
                                            <li className="page-item">
                                                <a href="#" className="page-link" aria-label="Previous">
                                                    <i className="ti-angle-left" />
                                                </a>
                                            </li>
                                            <li className="page-item">
                                                <a href="#" className="page-link">1</a>
                                            </li>
                                            <li className="page-item active">
                                                <a href="#" className="page-link">2</a>
                                            </li>
                                            <li className="page-item">
                                                <a href="#" className="page-link" aria-label="Next">
                                                    <i className="ti-angle-right" />
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="blog_right_sidebar">
                                    <aside className="single_sidebar_widget search_widget">
                                        <form action="#">
                                            <div className="form-group">
                                                <div className="input-group mb-3">
                                                    <input type="text" className="form-control" placeholder="Search Keyword" />
                                                    <div className="input-group-append">
                                                        <button className="btns" type="button"><i className="ti-search" /></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <button className="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" type="submit">Search</button>
                                        </form>
                                    </aside>
                                    <aside className="single_sidebar_widget post_category_widget">
                                        <h4 className="widget_title" style={{ color: '#2d2d2d' }}>Catergorias</h4>
                                        <ul className="list cat-list">
                                                    <li>
                                                        <a style={{cursor:'pointer'}}  className="d-flex" onClick={(e)=>{this.selectCategory(null)}}>
                                                            <p>Todas las Categorias</p>
                                                        </a>
                                                    </li>
                                            {
                                                categorys.map((data,key)=>{
                                                    return <li key={`article_${key}`}>
                                                        <a style={{cursor:'pointer'}} className="d-flex" onClick={()=>{this.selectCategory(data)}}>
                                                            <p>{data.name}</p>
                                                        </a>
                                                    </li>
                                                })
                                            }
                                        </ul>
                                    </aside>
                                    <aside className="single_sidebar_widget popular_post_widget">
                                        <h3 className="widget_title" style={{ color: '#2d2d2d' }}>Publicaciones Recientes</h3>

                                        {
                                            publicationsLimitTwo.map((data,key)=>{
                                                return <div key={`article_${key}`} className="media post_item">
                                                            
                                                            {data.typepub == 'video'? 
                                                                (
                                                                    <img src='/principal/img/videoicon.jpg'style={{width:'35%'}}/>
                                                                )
                                                                :
                                                                <img src={data.urlfile} style={{width:'35%'}}/>
                                                            }
                                                            <div className="media-body">
                                                                <a href="blog_details.html">
                                                                    <h3 style={{ color: '#2d2d2d' }}>{data.title}</h3>
                                                                </a>
                                                                <p>{moment(data.created_view).format('MMMM DD, YYYY')}</p>
                                                            </div>
                                                        </div>
                                            })
                                        }
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}



export default withTracker((props)=>{
    const optiospublications = {
        category : categorySelect.get()
    }
    const subscriptionPublications = Meteor.subscribe('publications',optiospublications,'getAllPublications')
    const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
    const categorys = categoryClass.find().fetch()
    const publications = publicationsClass.find({},{sort:{createdAt:-1}}).fetch()
    const publicationsLimitTwo = publicationsClass.find({},{sort:{createdAt:-1},limit:2}).fetch()
    return {publications,subscriptionPublications,categorys,subcriptionCategory,publicationsLimitTwo}
})(Main)