import React, { Component } from 'react'
import Principal from '/imports/view/principal/Principal'
import Main from '/imports/view/principal/Main'
import Signin from '/imports/view/auth/Signin'
import Signup from '/imports/view/auth/Signup'
import Layout from '/imports/view/layout/Layout'
import Home from '/imports/view/home/Home'
import {useLocation} from "react-router-dom";
import { createBrowserHistory } from "history"

import CategoryRoutes from './CategoryRoutes'
import PublicationsRoutes from './PublicationsRoutes'
const history = createBrowserHistory();

function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        La pagina solicitada no existe <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

export const routes = [//un array de objectos donde estan mis rutas con mis componentes que quiero que renderice 
    {
      path:'/bienvenido',
      component:Principal,
      authenticated:false,
      routes:[
        {
          path:'/bienvenido/principal',
          component:Main
        },
        {
          path:'/bienvenido/login',
          component:Signin
        },
        {
          path:'/bienvenido/registro',
          component:Signup
        }
      ]
    },
    {
        path:'/dashboard',//ruta
        component:Layout,//componente
        authenticated:true,
        routes: [
            {
              path: "/dashboard/home",
              component: Home
            },
            ...PublicationsRoutes,
            ...CategoryRoutes
        ]
    },
    {
      path:'/otraruta',//ruta
      component:Layout,//componente
      authenticated:true
    },
    {
      path:'*',
      component:NoMatch
    }
]

function getAllRoutesAutheticated(routes){
  let routesAuthenticate = []
  routes.forEach((value,index) => {
      if(JSON.stringify(value.authenticated) === JSON.stringify(true)){
        routesAuthenticate.push(value.path)
        if(value.routes){
          value.routes.forEach((v,i) => {
            routesAuthenticate.push(v.path)
          });
        }
      }
  });
  return routesAuthenticate
}

export const checkAuthUser = function(authenticated){
    //console.log('el usuario esta autenticado:'+ authenticated)
    const path = history.location.pathname
    const isAuthenticatedPage = getAllRoutesAutheticated(routes).includes(path)
    if(authenticated && isAuthenticatedPage){
        //console.log('el usuario puede ingresar a las paginas que necesitan authenticacion')
        history.replace('/dashboard/home')
    }else if(!authenticated && isAuthenticatedPage){
      //console.log('el usuario no esta logueado')
      history.replace('/')
      location.reload()
    }
}