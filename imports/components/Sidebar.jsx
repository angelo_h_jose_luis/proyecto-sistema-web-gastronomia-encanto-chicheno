import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import {Roles} from 'meteor/alanning:roles'
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data'

class Sidebar extends Component {
    render() {
        const {createpublication,allpublicationuser,createCategory,viewallCategory} = this.props
        return (
            <div>
                <div className="main-sidebar sidebar-style-2">
                    <aside id="sidebar-wrapper">
                        <div className="sidebar-brand">
                            <Link to="/dashboard/home">
                                <img alt="image" src="/dashboard/img/logo.png" className="header-logo" /> <span className="logo-name">Otika</span>
                            </Link>
                        </div>
                        <ul className="sidebar-menu">
                            <li className="menu-header">Main</li>
                            <li className="dropdown active">
                                <Link className="nav-link" to="/dashboard/home"><i data-feather="monitor"></i><span>Panel de Control</span></Link>
                            </li>
                            <li className="menu-header">Publicaciones</li>
                            {createpublication?
                                        <li>
                                            <Link className="nav-link" to="/dashboard/create-publications"><i data-feather="monitor"></i><span>Crear Publicacion</span></Link>
                                        </li>
                                        :null}
                            
                            {allpublicationuser?
                                        <li>
                                            <Link className="nav-link" to="/dashboard/user-publications"><i data-feather="monitor"></i><span>Mis Publicacion</span></Link>
                                        </li>
                                        :null}

                            <li className="menu-header">Categorias</li>
                            {createCategory?
                                        <li>
                                        <Link className="nav-link" to="/dashboard/create-category">Crear Categoria</Link>
                                    </li>
                                        :null}

                            {viewallCategory?
                                         <li>
                                         <Link className="nav-link" to="/dashboard/all-category">Ver mis Categorias</Link>
                                     </li>
                                        :null}
                            
                        </ul>
                    </aside>
                </div>

            </div>
        )
    }
}



export default withTracker((props)=>{
    const createpublication = Roles.userIsInRole(Meteor.userId(),"createpublication")
    const allpublicationuser = Roles.userIsInRole(Meteor.userId(),"allpublicationuser")
    const createCategory = Roles.userIsInRole(Meteor.userId(),"createCategory")
    const viewallCategory = Roles.userIsInRole(Meteor.userId(),"viewallCategory")
    return {createpublication,allpublicationuser,createCategory,viewallCategory}
})(Sidebar)